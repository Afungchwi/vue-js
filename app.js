new Vue({
    el:'#vue-app',
    data:{
        name:'Shaun',
        job:'Ninja',
        website:'http://www.thenetninja.co.uk',
        websiteTag: '<a href="http://www.thenetninja.co.uk">The Net Ninja Website </a>',
        name:'',
        x:0,
        y:0,
        age:'21',
        a:0,
        b:0,
        available:true,
        nearby:false,
        error: false,
        success:false, 
        name:'jack',
        characters:['Mario','Luigi','Yoshi', 'Bowser'],
        ninjas:[
            {name:'Ryu', age:25},
            {name:'Yoshi', age:75},
            {name:'Ken', age:35}
        ]
    },
    methods:{
        greet: function(time){
            return 'Good' + time + this.name;
        },
        Add: function(inc){
            this.age+=inc;
        },
        Sub: function(dec){
            this.age-=dec;
        },
        updateXY: function(event){
            this.x = event.offsetX;
            this.y = event.offsetY;
        },
        click: function(){
            alert('You Clicked Me');
        },
        logName: function(){
            console.log('You entered Your Name');
        },
        logAge: function(){
            console.log('You entered Your Age');
        },
        logAge:function(){
            console.log("you entered your age");
        },
        addToA:function(){
            console.log('addToA');
            return this.a + this.age;
            
        },
        addToB:function(){
            console.log('addToB');
            return this.b + this.age;
           
        }
      },
      
       computed:{
        addToA:function(){
            console.log('addToA');
            return this.a + this.age;
            
        },
        addToB:function(){
            console.log('addToB');
            return this.b + this.age;
           
        },
        compClasses:function(){
            return {
                available:this.available,
                nearby:this.nearby
            }
        
        }
    }
});
var one = new Vue({
    el:'#vue-app-one',
    data:{
       title:'Vue App One'
    },
    methods:{

    },
    computed:{
     greet:function(){
         return 'Hello from app one'
     }
    }
});
var two = new Vue({
    el:'#vue-app-two',
    data:{
        title:'Vue App Two'
    },
    methods:{
      changeTitle:function(){
          one.title="Title changed"
      }
    },
    computed:{
    greet:function(){
       return 'Hello from app two'
}
    }
});
two.title = "Changed from outside";

Vue.component('greeting',{
    template:'<p>Hey there how are you? im {{name}}.<button v-on:click="changeName">change name</button></p>',
    data:function(){
        return {
            name: 'Yoshi' 
        }
    },
    methods:{
        changeName:function(){
            this.name = 'Mario';
        }
    }
});
new Vue({
    el:'#vue-app-three'
});
new Vue({
    el:'#vue-app-four'
});
new Vue({
    el:'#app2',
    data:{
        output:'best meal'
    },
    methods:{
        readRefs:function(){
            console.log(this.$refs.test.innerText);
            this.output = this.$refs.input.value;
        }
    }
});